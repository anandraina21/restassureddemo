package restassured.reqres;

import java.util.HashMap;
import java.util.Map;
import static org.hamcrest.Matchers.*;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import restassured.sample.Constants;
import restassured.sample.Setup;

public class TestCasesFile extends Setup {

	/**
	 * 1. Hit the GET API URL to list users 
	 * 2. Print response in console 
	 * 3. Validate response code 
	 * 4. Extract email response from JSON where id=2 
	 * 5. Print JSON size to console
	 */
	@Test(priority = 1)
	public void test_001_get_request() {
		System.out.println("===============test_001_get_request started===============\n");
		Response res = req.baseUri(Constants.BASE_URL + Constants.LIST_USERS).contentType("application/json").param("page", "1").get();
		res.then().assertThat().statusCode(200);
		int statusCode = res.getStatusCode();

		System.out.println("Status Code: " + statusCode + "\n");
		System.out.println("Response body: " + res.asPrettyString() + "\n");

		Map<String, Object> jsonResponse;
		jsonResponse = res.jsonPath().getMap("$");
		System.out.println("JsonResponse Size: " + jsonResponse.size() + "\n");
		for (int i = 0; i < jsonResponse.size(); i++) {
			jsonResponse = res.jsonPath().getMap("data[" + i + "]");
			if ((int)jsonResponse.get("id") == 2) {
				System.out.println("JSON Response for object where id=2: " + jsonResponse + "\n");
				System.out.println("Email for object where id=2: " + jsonResponse.get("email") + "\n");
				break;
			}
		}
		System.out.println("===============test_001_get_request completed===============\n");
	}
	
	/**
	 * 1. Hit the POST API URL to create user
	 * 2. Print response in console
	 * 3. Validate response code
	 * 4. Validate response body
	 */
	@Test(priority=2)
	public void test_002_post_request() {
		System.out.println("===============test_002_post_request started===============\n");
		RequestSpecification req = RestAssured.given();
		Response res = req.baseUri(Constants.BASE_URL).contentType("application/json").body("{"
				+ "    \"name\": \"andromeda\","
				+ "    \"job\": \"abcd\""
				+ "}").post(Constants.CREATE_USER);
		res.then().assertThat().statusCode(201);
		int statusCode = res.getStatusCode();
		
		System.out.println("Status Code: " + statusCode + "\n");
		String jsonResponseOutput = res.asPrettyString();
		System.out.println("Response body: " + jsonResponseOutput + "\n");
		TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {};
		try {
			Map<String, Object> map = new ObjectMapper().readValue(jsonResponseOutput, typeRef);
			if(map.get("name").equals("andromeda") && map.get("job").equals("abcd")) {
				System.out.println("Response body validated successfully!");
			} else {
				System.out.println("Response body validation error!");
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("===============test_002_post_request completed===============\n");
	}
	
	/**
	 * 1. Hit the PATCH API URL to update user
	 * 2. Print response in console
	 * 3. Validate response code
	 * 4. Validate response body using Hamcrest matchers
	 */
	@Test(priority=3)
	public void test_003_patch_request() {
		System.out.println("===============test_003_patch_request started===============\n");
		Response res = req.baseUri(Constants.BASE_URL + Constants.UPDATE_USER).contentType("application/json").body("{"
				+ "    \"name\": \"andromeda\","
				+ "    \"job\": \"hhhhkkkk\""
				+ "}").patch();
		res.then().assertThat().statusCode(200);
		int statusCode = res.getStatusCode();
		
		System.out.println("Status Code: " + statusCode + "\n");
		System.out.println("Response body: " + res.asPrettyString() + "\n");
		
		res.then().assertThat().body("name", equalTo("andromeda"));
		res.then().assertThat().body("job", equalTo("hhhhkkkk"));
		System.out.println("===============test_003_patch_request completed===============\n");
	}
	
	/**
	 * 1. Hit the DELETE API URL to delete user
	 * 2. Print response in console
	 * 3. Validate response code
	 */
	@Test(priority=4)
	public void test_004_delete_request() {
		System.out.println("===============test_004_delete_request started===============\n");
		Response res = req.baseUri(Constants.BASE_URL + Constants.DELETE_USER).contentType("application/json").delete();
		res.then().assertThat().statusCode(204);
		int statusCode = res.getStatusCode();
		
		System.out.println("Status Code: " + statusCode + "\n");
		System.out.println("Response body: " + res.asPrettyString() + "\n");
		System.out.println("===============test_004_delete_request completed===============\n");
	}

}
