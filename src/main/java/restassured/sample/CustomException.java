package restassured.sample;

/**
 * 
 * Throws a user defined custom exception.
 */
@SuppressWarnings("serial")
public class CustomException extends Exception {
	public CustomException(String exceptionText) {
		super(exceptionText);
	}
}
