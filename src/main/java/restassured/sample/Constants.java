package restassured.sample;

public class Constants 
{
	private Constants() {
		
	}
    public final static String BASE_URL = "https://reqres.in";
    
    public final static String LIST_USERS = "/api/users";
    
    public final static String CREATE_USER = "/api/users";
    
    public final static String UPDATE_USER = "/api/users/2";
    
    public final static String DELETE_USER = "/api/users/2";
    
    public final static String BEERS_BASE_URL = "https://api.punkapi.com/v2/beers";
}
